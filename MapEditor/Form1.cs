﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class Form1 : Form
    {
        internal class Selection
        {
            public byte width;
            public byte height;
            public short[] selectedItems;
            public Selection(byte w, short[]selection)
            {
                this.width = w;
                this.height = (byte)(selection.Length / w);
                this.selectedItems = selection;
            }
            public Selection(short selection)
            {
                this.selectedItems = new short[] { selection };
                this.width = 1;
                this.height = 1;
            }
            public void Add(short id)
            {
                short[] newarray = new short[selectedItems.Length + 1];
                int i = 0;
                while (i < selectedItems.Length)
                    newarray[i] = selectedItems[i++];
                newarray[i] = id;
            }
        }
        internal Selection selection;
        internal MapEditorXNA game;
        internal string savePath = "";

        public Form1()
        {
            InitializeComponent();
        }
        public void OnImageClick(object sender, System.EventArgs args)
        {
            PictureBox pb = (PictureBox)sender;
            var keyState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            if (!keyState.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.LeftShift))
            {
                foreach (TabPage tp in tabTileSets.Controls)
                {
                    foreach (PictureBox pb2 in tp.Controls)
                    {
                        pb2.BorderStyle = BorderStyle.FixedSingle;
                        if (pb2.Controls.Count != 0)
                            pb2.Controls.RemoveAt(0);
                    }
                }
                foreach (TabPage tp in tabAnimated.Controls)
                {
                    foreach (PictureBox pb2 in tp.Controls)
                    {
                        pb2.BorderStyle = BorderStyle.FixedSingle;
                        if (pb2.Controls.Count != 0)
                            pb2.Controls.RemoveAt(0);
                    }
                }  
                selection = null;
            }
            PictureBox newPB = new PictureBox();
            Bitmap bm = new System.Drawing.Bitmap(16, 16);
            for (int k = 0; k < 16; k++)
                for (int j = 0; j < 16; j++)
                    bm.SetPixel(k, j, System.Drawing.Color.FromArgb(30, 250, 50, 50));
            newPB.Image = bm;            
            newPB.Size = pb.Size;
            newPB.SizeMode = PictureBoxSizeMode.Zoom;
            newPB.BackColor = Color.Transparent;
            newPB.Name = "overlay";            
            pb.Controls.Add(newPB);

            if (selection == null)
                selection = new Selection((short)pb.Tag);
            else if (!selection.selectedItems.Contains((short)pb.Tag))
                selection.Add((short)pb.Tag);
            pb.BorderStyle = BorderStyle.Fixed3D;
            pbEraser.BorderStyle = BorderStyle.FixedSingle;
        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            savePath = "";
            mapNameLabel.Text = "";
            game.level = new Level();
            game.level.Name = "";
            game.level.layers = new Layer[0];
            listLayers.Items.Clear();
            game.level.width = 32;
            game.level.height = 32;
            widthBox.Text = "32";
            heightBox.Text = "32";
            game.level.tileSets = game.tileSets;
            game.level.animatedSets = game.animatedSets;
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (savePath != "")
            {
                SaveMap(savePath);
            }
            else
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Title = "Choose file to save to";
                saveDialog.DefaultExt = "lvl";
                saveDialog.Filter = "Map Files|*.lvl";
                saveDialog.RestoreDirectory = true;
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    savePath = saveDialog.FileName;
                    mapNameLabel.Text = savePath;
                    SaveMap(savePath);
                }
            }
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Map Files|*.lvl";
            openFileDialog1.Title = "Select a Level File";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string path = openFileDialog1.FileName;
                if (ReadMap(path))
                {
                    savePath = path;
                    mapNameLabel.Text = savePath;
                    widthBox.Text = game.level.width.ToString();
                    heightBox.Text = game.level.height.ToString();
                }
            }
        }

        public void SaveMap(string path)
        {
            byte MapVersion = 1;
            try
            {
                using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create))
                {
                    using (System.IO.BinaryWriter writer = new System.IO.BinaryWriter(fs, Encoding.UTF8))
                    {
                        writer.Write(MapVersion);
                        writer.Write(game.level.Name);
                        writer.Write(game.level.width);
                        writer.Write(game.level.height);
                        //writer.Write(game.level.playerSpawn.X);
                        //writer.Write(game.level.playerSpawn.Y);
                        writer.Write(game.level.tileSets.Length);
                        for (int i = 0; i < game.level.tileSets.Length; i++)
                        {
                            writer.Write(game.level.tileSets[i].texture.Name);
                            writer.Write(game.level.tileSets[i].tileW);
                            writer.Write(game.level.tileSets[i].tileH);
                            writer.Write(game.level.tileSets[i].firstGID);
                        }
                        writer.Write(game.level.animatedSets.Length);
                        for (int i = 0; i < game.level.animatedSets.Length; i++)
                        {
                            writer.Write(game.level.animatedSets[i].texture.Name);
                            writer.Write(game.level.animatedSets[i].tileW);
                            writer.Write(game.level.animatedSets[i].tileH);
                            writer.Write(game.level.animatedSets[i].frameTime);
                        }
                        writer.Write(game.level.layers.Length);
                        for (int i = 0; i < game.level.layers.Length; i++)
                        {
                            writer.Write((byte)game.level.layers[i].type);
                            for (int j = 0; j < game.level.layers[i].gIDs.Length; j++)
                                writer.Write(game.level.layers[i].gIDs[j]);
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
        }
        public bool ReadMap(string path)
        {
            try
            {
                using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open))
                {
                    using (System.IO.BinaryReader reader = new System.IO.BinaryReader(fs, Encoding.UTF8))
                    {
                        byte MapVersion = reader.ReadByte();
                        game.level.Name = reader.ReadString();
                        game.level.width = reader.ReadInt16();
                        game.level.height = reader.ReadInt16();

                        game.level.tileSets = new TileCollection[reader.ReadInt32()];
                        for (int i = 0; i < game.level.tileSets.Length; i++)
                        {
                            game.level.tileSets[i] = new TileCollection(game.Content, game.GraphicsDevice, reader.ReadString(), reader.ReadByte(), reader.ReadByte(), reader.ReadInt16());
                        }
                        game.level.animatedSets = new AnimatedTileSet[reader.ReadInt32()];
                        for (int i = 0; i < game.level.animatedSets.Length; i++)
                        {
                            game.level.animatedSets[i] = new AnimatedTileSet(game.Content, reader.ReadString(), reader.ReadByte(), reader.ReadByte(), reader.ReadSingle());
                        }
                        game.level.layers = new Layer[reader.ReadInt32()];
                        for (int i = 0; i < game.level.layers.Length; i++)
                        {
                            game.level.layers[i] = new Layer();
                            game.level.layers[i].type = (LayerType)reader.ReadByte();
                            game.level.layers[i].gIDs = new short[game.level.width * game.level.height];
                            for (int j = 0; j < game.level.layers[i].gIDs.Length; j++)
                                game.level.layers[i].gIDs[j] = reader.ReadInt16();
                        }
                    }
                }
                RefreshLayerList();
                return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return false;
        }

        private void mapSizeButton_Click(object sender, EventArgs e)
        {
            short w, h;
            if (short.TryParse(widthBox.Text, out w) && short.TryParse(heightBox.Text, out h))
            {
               // Console.WriteLine("resizing map");
                game.ResizeMap(w, h);
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            pbPencil.Image = imageList1.Images["pencil.png"];
            pbSelection.Image = imageList1.Images["selection.png"];
            pbFillTool.Image = imageList1.Images["fill.png"];
            pbEraser.Image = imageList1.Images["eraser.png"];
        }


        private void listLayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            var list = (ListBox)sender;
            if (list.SelectedIndex != -1)
            {
                var layer = game.level.layers[list.SelectedIndex];
                comboLayerTypes.SelectedItem = layer.type;
                //selection = null;
                checkLayerVisible.Checked = layer.visible;
            }
        }

        private void listLayers_DoubleClick(object sender, EventArgs e)
        {
            ListBox list = (ListBox)sender;
            if (list.SelectedItem == null)
                return;

            TextBox tb = new TextBox();
            tb.Location = new Point(0, list.SelectedIndex * DefaultFont.Height);
            
            tb.Visible = true;
            tb.Enabled = true;
            tb.Size = new Size(100, 30);
            tb.Text = list.SelectedItem.ToString();
            tb.Parent = list;
            tb.LostFocus += tbLoseFocus;
            tb.Focus();
        }
        private void tbLoseFocus(object sender, EventArgs e)
        {
            var tb = (TextBox)sender;
            if (tb.Text != "")
                listLayers.Items[listLayers.SelectedIndex] = tb.Text;
            listLayers.Controls.Remove(tb);
            tb.Dispose();
        }
        private void bLayerAdd_Click(object sender, EventArgs e)
        {
            Layer[] layers = new Layer[game.level.layers.Length + 1];
            for (int i = 0; i < game.level.layers.Length; i++)
                layers[i] = game.level.layers[i];
            Layer layer = new Layer();
            layer.type = LayerType.Floor;
            layer.gIDs = new short[game.level.width * game.level.height];
            for (int i = 0; i < layer.gIDs.Length; i++)
                layer.gIDs[i] = -1;
            layers[layers.Length - 1] = layer;
            game.level.layers = layers;
            listLayers.Items.Add(String.Format("layer{0}", layers.Length));
        }
        private void bLayerRem_Click(object sender, EventArgs e)
        {
            if (listLayers.SelectedIndex != -1)
            {
                Layer[] layers = new Layer[game.level.layers.Length - 1];
                
                for (int i = 0, j=0; i < layers.Length; i++,j++)
                {
                    if (i == listLayers.SelectedIndex)
                        j++;
                    layers[i] = game.level.layers[j];
                }
                game.level.layers = layers;
                listLayers.Items.RemoveAt(listLayers.SelectedIndex);
            }
        }
        private void bLayerUp_Click(object sender, EventArgs e)
        {
            if (listLayers.SelectedIndex == 0)
                return;
            int i1 = listLayers.SelectedIndex;
            int i2 = i1 - 1;
            var temp = game.level.layers[i2];
            game.level.layers[i2] = game.level.layers[i1];
            game.level.layers[i1] = temp;
            listLayers.SelectedIndex = i2;
            var temp2 = listLayers.Items[i2];
            listLayers.Items[i2] = listLayers.Items[i1];
            listLayers.Items[i1] = temp2;
        
        }
        private void bLayerD_Click(object sender, EventArgs e)
        {
            if (listLayers.SelectedIndex == listLayers.Items.Count - 1)
                return;
            int i1 = listLayers.SelectedIndex;
            int i2 = i1 + 1;
            var temp = game.level.layers[i2];
            game.level.layers[i2] = game.level.layers[i1];
            game.level.layers[i1] = temp;
            listLayers.SelectedIndex = i2;
            var temp2 = listLayers.Items[i2];
            listLayers.Items[i2] = listLayers.Items[i1];
            listLayers.Items[i1] = temp2;
  
        }
        private void comboLayerTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var box = (ComboBox)sender;
            if (listLayers.SelectedIndex != -1 && box.SelectedIndex != -1)
            {
                if ((LayerType)box.SelectedItem == LayerType.Animated)
                {
                    tabTileSets.Enabled = false;
                    tabTileSets.Visible = false;
                    tabAnimated.Enabled = true;
                    tabAnimated.Visible = true;
                }
                else
                {
                    tabTileSets.Enabled = true;
                    tabTileSets.Visible = true;
                    tabAnimated.Enabled = false;
                    tabAnimated.Visible = false;
                }
                if (game.level.layers[listLayers.SelectedIndex].type != (LayerType)box.SelectedItem)
                {
                    if ((LayerType)box.SelectedItem == LayerType.Animated)
                    {
                        for (int i = 0; i < game.level.layers[listLayers.SelectedIndex].gIDs.Length; i++)
                        {
                            if (game.level.layers[listLayers.SelectedIndex].gIDs[i] >= game.level.animatedSets.Length)
                                game.level.layers[listLayers.SelectedIndex].gIDs[i] = -1;
                        }
                    }
                    game.level.layers[listLayers.SelectedIndex].type = (LayerType)box.SelectedItem;
                }
            }
        }

        void RefreshLayerList()
        {
            listLayers.Items.Clear();
            for (int i = 0; i < game.level.layers.Length; i++)
            {
                listLayers.Items.Add(i);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            var cb = sender as CheckBox;
            if (listLayers.SelectedIndex != -1)
            {
                game.level.layers[listLayers.SelectedIndex].visible = cb.Checked;
            }
        }

        private void pbEraser_Click(object sender, EventArgs e)
        {
            pbPencil.BorderStyle = BorderStyle.FixedSingle;
            pbSelection.BorderStyle = BorderStyle.FixedSingle;
            pbFillTool.BorderStyle = BorderStyle.FixedSingle;
            pbEraser.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pbPencil_Click(object sender, EventArgs e)
        {
            pbPencil.BorderStyle = BorderStyle.Fixed3D;
            pbSelection.BorderStyle = BorderStyle.FixedSingle;
            pbFillTool.BorderStyle = BorderStyle.FixedSingle;
            pbEraser.BorderStyle = BorderStyle.FixedSingle;
        }

        private void pbSelection_Click(object sender, EventArgs e)
        {
            pbPencil.BorderStyle = BorderStyle.FixedSingle;
            pbSelection.BorderStyle = BorderStyle.Fixed3D;
            pbFillTool.BorderStyle = BorderStyle.FixedSingle;
            pbEraser.BorderStyle = BorderStyle.FixedSingle;
        }

        private void pbFillTool_Click(object sender, EventArgs e)
        {
            pbPencil.BorderStyle = BorderStyle.FixedSingle;
            pbSelection.BorderStyle = BorderStyle.FixedSingle;
            pbFillTool.BorderStyle = BorderStyle.Fixed3D;
            pbEraser.BorderStyle = BorderStyle.FixedSingle;
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FromTileset form = new FromTileset(game.AddTileset);
            form.Enabled = true;
            form.Visible = true;
        }



    }
}
