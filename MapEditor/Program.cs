using System;

namespace MapEditor
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            using (MapEditorXNA game = new MapEditorXNA())
            {
                game.Run();
            }
        }
    }
#endif
}

