﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class FromTileset : Form
    {
        Action<Form> Callback;
        public FromTileset(Action<Form> callback)
        {
            InitializeComponent();
            this.Callback = callback;
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files|*.jpg;*.png;*.gif";
            dialog.Title = "Select the tile set file";
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string path = dialog.FileName;
                TextBox box = Controls["pathBox"] as TextBox;
                if (box != null)
                    box.Text = path;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (Callback != null)
                Callback(this);
        }

    }
}
