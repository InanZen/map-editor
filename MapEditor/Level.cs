﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace MapEditor
{
    public class Level
    {
        public string Name;
        public short width;
        public short height;
        public Point playerSpawn;
        public Layer[] layers;
        public ActionLayer[] actionLayers;
        public TileCollection[] tileSets;
        public AnimatedTileSet[] animatedSets;
        public TileCollection getTileSetFromGID(int gID)
        {
            for (int i = tileSets.Length - 1; i >= 0; i--)
                if (gID >= tileSets[i].firstGID)
                    return tileSets[i];
            return null;
        }
    }

    public class Layer
    {
        public LayerType type;
        public short[] gIDs;
        public bool visible = true;
    }
    public enum LayerType
    {
        Animated = 0,
        Floor = 1,
        Scenery = 2,
        Collision = 3,
        Floating = 4
    }

    public class TileCollection
    {
        public Texture2D texture;
        public byte tileW;
        public byte tileH;
        public int wTileCount { get { return texture.Width / tileW; } }
        public int hTileCount { get { return texture.Height / tileH; } }
        public int elementCount { get { return wTileCount * hTileCount; } }
        public short firstGID;

        public TileCollection(ContentManager content, GraphicsDevice device, string Texture, byte TileW, byte TileH, short FirstGID)
        {
            string path = Path.Combine(content.RootDirectory, "TileSets", Texture);
            Console.WriteLine("path: {0}", path);
            if (File.Exists(path))
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    this.texture = Texture2D.FromStream(device, stream);
                    this.texture.Name = Texture;
                }
            }
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
        public TileCollection(ContentManager content, Texture2D Texture, byte TileW, byte TileH, short FirstGID)
        {
            this.texture = Texture;
            this.texture.Name = Texture.Name;
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
    }
    public class AnimatedTileSet
    {
        public Texture2D texture;
        public byte tileW;
        public byte tileH;
        public byte FrameIndex;
        public float frameTime;
        private float TotalTime = 0;
        public int FrameCount { get { return texture.Width / tileW; } }

        public AnimatedTileSet(ContentManager content, string Texture, byte TileW, byte TileH, float FrameTime)
        {
            this.texture = content.Load<Texture2D>(Texture);
            this.texture.Name = Texture;
            this.tileW = TileW;
            this.tileH = TileH;
            this.frameTime = FrameTime;
        }
        public AnimatedTileSet(ContentManager content, Texture2D Texture, byte TileW, byte TileH, float FrameTime)
        {
            this.texture = Texture;
            this.texture.Name = Texture.Name;
            this.tileW = TileW;
            this.tileH = TileH;
            this.frameTime = FrameTime;
        }

        public void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            TotalTime += elapsed;
            if (TotalTime > frameTime)
            {
                FrameIndex++;
                FrameIndex = (byte)(FrameIndex % FrameCount);
                TotalTime = 0;
            }
        }
    }
}
