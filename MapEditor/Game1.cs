using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Windows.Forms;

namespace MapEditor
{

    public class ActionLayer
    {
        public ActionEvent[] data;
    }
    public class ActionEvent
    {
        public virtual void Clicked() { }
        public virtual void Update() { }
    }
    public class SpawnEvent : ActionEvent
    {
        public Point location;
    }
    
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MapEditorXNA : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        VertexPositionColor[] gridVertices;
        BasicEffect effect;

        Form1 form;
        internal int zoom = 32;
        int startX = 0;
        int startY = 0;


        Texture2D mouseCursor;
        MouseState currentMouseState;

        public Level level;

        public TileCollection[] tileSets;
        public AnimatedTileSet[] animatedSets;


        public MapEditorXNA()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            form = new Form1();
            form.Show();
            form.game = this;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            effect = new BasicEffect(GraphicsDevice);

            mouseCursor = Content.Load<Texture2D>("cursor");

            level = new Level { width = 32, height = 32, Name = "", playerSpawn = new Point(16, 16), layers = new Layer[0]};           

            tileSets = new TileCollection[0];
          /*  tileSets[0] = new TileCollection(Content, "tiles1", 16, 16, 0);
            tileSets[1] = new TileCollection(Content, "tiles2", 16, 16, (short)(tileSets[0].firstGID + tileSets[0].elementCount));
            tileSets[2] = new TileCollection(Content, "tiles3", 16, 16, (short)(tileSets[1].firstGID + tileSets[1].elementCount));
            tileSets[3] = new TileCollection(Content, "tiles4", 16, 16, (short)(tileSets[2].firstGID + tileSets[2].elementCount));*/
            level.tileSets = tileSets;

            animatedSets = new AnimatedTileSet[0];
          /*  animatedSets[0] = new AnimatedTileSet(Content, "water", 32, 32, 0.15f);
            animatedSets[1] = new AnimatedTileSet(Content, "fire", 64, 64, 0.15f);
            animatedSets[2] = new AnimatedTileSet(Content, "lava", 32, 32, 0.35f);*/
            level.animatedSets = animatedSets;

            form.comboLayerTypes.Items.Add(LayerType.Animated);
            form.comboLayerTypes.Items.Add(LayerType.Floor);
            form.comboLayerTypes.Items.Add(LayerType.Scenery);
            form.comboLayerTypes.Items.Add(LayerType.Collision);
            form.comboLayerTypes.Items.Add(LayerType.Floating);

            System.Windows.Forms.ImageList imgList = new System.Windows.Forms.ImageList();
            imgList.ImageSize = new System.Drawing.Size(16, 16);
            imgList.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
            imgList.TransparentColor = System.Drawing.Color.Transparent;
            form.tabTileSets.ImageList = imgList;
            form.tabAnimated.Name = "animated";
            CopyTileSetsToForm();


            

            int w = (GraphicsDevice.Viewport.Width/zoom) * 2;
            int h = (GraphicsDevice.Viewport.Height/zoom) * 2;
            gridVertices = new VertexPositionColor[w+h];
            for (int i = 0; i < w; i += 2)
            {
                float x = (1f / GraphicsDevice.Viewport.Width) * zoom * i - 1;
                gridVertices[i] = new VertexPositionColor(new Vector3(x, 1, 0), Color.Black);
                gridVertices[i + 1] = new VertexPositionColor(new Vector3(x, -1, 0), Color.Black);
            }
            for (int i = 0; i < h; i += 2)
            {
                float y = (1f / GraphicsDevice.Viewport.Height) * zoom * i - 1;
                gridVertices[w + i] = new VertexPositionColor(new Vector3(1, y, 0), Color.Black);
                gridVertices[w + i + 1] = new VertexPositionColor(new Vector3(-1, y, 0), Color.Black);
            }
        }
        private System.Drawing.Bitmap CreateBitmap(Texture2D texture)
        {
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                texture.SaveAsJpeg(ms, texture.Width, texture.Height);
                return new System.Drawing.Bitmap(ms);
            }
        }
        public void CopyTileSetsToForm()
        {
            var removeButton = form.removeToolStripMenuItem;

            form.tabTileSets.Controls.Clear();
            form.tabTileSets.ImageList.Images.Clear();
            List<ToolStripMenuItem> removeItems = new List<ToolStripMenuItem>();
            for (int i = 0; i < tileSets.Length; i++)
            {
                ToolStripMenuItem removeItem = new ToolStripMenuItem(tileSets[i].texture.Name);
                removeItem.Name = tileSets[i].texture.Name;
                removeItem.Click += RemoveTileset;
                removeItem.Tag = false;
                removeItems.Add(removeItem);
                short gID = tileSets[i].firstGID;
                var tabPage = new System.Windows.Forms.TabPage() { Name = tileSets[i].texture.Name, Text = tileSets[i].texture.Name };
                for (int y = 0; y < tileSets[i].hTileCount; y++)
                {
                    for (int x = 0; x < tileSets[i].wTileCount; x++)
                    {
                        byte w = tileSets[i].tileW;
                        byte h = tileSets[i].tileH;
                        Color[] colors = new Color[w * h];
                        tileSets[i].texture.GetData(0, new Rectangle(x * w, y * h, w, h), colors, 0, w * h);
                        System.Drawing.Bitmap bm = new System.Drawing.Bitmap(w, h);
                        for (int k = 0; k < w; k++)
                            for (int j = 0; j < h; j++)
                                bm.SetPixel(k, j, System.Drawing.Color.FromArgb(colors[k + j * w].A, colors[k + j * w].R, colors[k + j * w].G, colors[k + j * w].B));
                        form.tabTileSets.ImageList.Images.Add(bm); // add image to imagelist
                        System.Windows.Forms.PictureBox pb = new System.Windows.Forms.PictureBox();
                        pb.Location = new System.Drawing.Point((x % tileSets[i].wTileCount) * 68, (y % tileSets[i].hTileCount) * 68);
                        pb.Size = new System.Drawing.Size(64, 64);
                        pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
                        pb.Image = form.tabTileSets.ImageList.Images[gID];
                        pb.Click += form.OnImageClick;
                        pb.Tag = gID;
                        pb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                        tabPage.Controls.Add(pb);
                        gID++;
                    }
                }
                tabPage.AutoScroll = true;
                form.tabTileSets.Controls.Add(tabPage);
            }        
            form.animatedPage.Controls.Clear();
            for (short i = 0; i < animatedSets.Length; i++)
            {
                var tileSet = animatedSets[i];
                ToolStripMenuItem removeItem = new ToolStripMenuItem(String.Format("{0} [anim]", tileSet.texture.Name));
                removeItem.Name = tileSet.texture.Name;
                removeItem.Click += RemoveTileset;
                removeItem.Tag = true;
                removeItems.Add(removeItem);
                Color[] colors = new Color[tileSet.texture.Width * tileSet.texture.Height];
                tileSet.texture.GetData(colors);
                System.Drawing.Bitmap bm = new System.Drawing.Bitmap(tileSet.texture.Width, tileSet.texture.Height);
                for (int k = 0; k < tileSet.texture.Width; k++)
                    for (int j = 0; j < tileSet.texture.Height; j++)
                        bm.SetPixel(k, j, System.Drawing.Color.FromArgb(colors[k + j * tileSet.texture.Width].A, colors[k + j * tileSet.texture.Width].R, colors[k + j * tileSet.texture.Width].G, colors[k + j * tileSet.texture.Width].B));
                System.Windows.Forms.PictureBox pb = new System.Windows.Forms.PictureBox();
                pb.Size = new System.Drawing.Size(form.tabTileSets.Width - 4, 48);
                pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
                pb.Image = bm;
                pb.Click += form.OnImageClick;
                pb.Tag = i;
                pb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                pb.Location = new System.Drawing.Point(2, i * 50 + 2);
                form.animatedPage.Controls.Add(pb);
            }
            removeButton.DropDownItems.Clear();
            removeButton.DropDownItems.AddRange(removeItems.ToArray());
        }
        public void AddTileset(Form addForm)
        {
            CheckBox animCB = addForm.Controls["checkBox1"] as CheckBox;
            TextBox pathBox = addForm.Controls["pathBox"] as TextBox;
            MaskedTextBox wBox = addForm.Controls["widthBox"] as MaskedTextBox;
            MaskedTextBox hBox = addForm.Controls["heightBox"] as MaskedTextBox;
            MaskedTextBox frameBox = addForm.Controls["frameTimeBox"] as MaskedTextBox;
            if (pathBox == null || animCB == null || wBox == null || hBox == null)            
                return;            
            string path = pathBox.Text;
           //Console.WriteLine("path: {0}", path);
            if (!System.IO.File.Exists(path))
                return;
            byte w = 0;
            byte h = 0;
            if (byte.TryParse(wBox.Text, out w) && w > 0 && byte.TryParse(hBox.Text, out h) && h > 0)
            {
                Texture2D texture = null;
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    texture = Texture2D.FromStream(GraphicsDevice, stream);
                    texture.Name = Path.GetFileName(path);
                }
                if (texture == null)
                    return;
                if (animCB.Checked)
                {
                    int miliSec = 0;
                    if (int.TryParse(frameBox.Text, out miliSec) && miliSec > 0)
                    {
                        float FrameTime = (float)miliSec / 1000;
                        AnimatedTileSet[] newSet = new AnimatedTileSet[animatedSets.Length + 1];
                        for (int i = 0; i < animatedSets.Length; i++)                        
                            newSet[i] = animatedSets[i];
                
                        newSet[animatedSets.Length] = new AnimatedTileSet(Content, texture, w, h, FrameTime);                        
                        animatedSets = newSet;
                        CopyTileSetsToForm();
                        level.animatedSets = animatedSets;
                    }
                }
                else
                {
                    TileCollection[] newSet = new TileCollection[tileSets.Length + 1];
                    short firstGid = 0;
                    for (int i = 0; i < tileSets.Length; i++)
                    {
                        newSet[i] = tileSets[i];
                        firstGid += (short)tileSets[i].elementCount;
                    }
                   // short firstGid = (short)(tileSets[tileSets.Length - 1].firstGID + tileSets[tileSets.Length - 1].elementCount); 
                    newSet[tileSets.Length] = new TileCollection(Content, texture, w, h, firstGid);
                    tileSets = newSet;
                    CopyTileSetsToForm();
                    level.tileSets = tileSets;
                }
            }
            addForm.Close();
        }
        public void RemoveTileset(Object sender, EventArgs args)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item != null)
            {
                if ((bool)item.Tag)
                {
                    List<AnimatedTileSet> newSets = new List<AnimatedTileSet>();
                    for (int i = 0; i < animatedSets.Length; i++)
                    {
                        if (animatedSets[i].texture.Name != item.Name)                        
                            newSets.Add(animatedSets[i]);                        
                    }
                    animatedSets = newSets.ToArray();
                }
                else
                {
                    List<TileCollection> newSets = new List<TileCollection>();
                    short firstGID = 0;
                    for (int i = 0; i < tileSets.Length; i++)
                    {
                        if (tileSets[i].texture.Name != item.Name)
                        {
                            tileSets[i].firstGID = firstGID;
                            firstGID += (short)tileSets[i].elementCount;
                            newSets.Add(tileSets[i]);
                        }
                    }
                    tileSets = newSets.ToArray();
                }
                CopyTileSetsToForm();
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keystate = Keyboard.GetState();
            currentMouseState = Mouse.GetState();
            if (System.Windows.Forms.Form.ActiveForm == (System.Windows.Forms.Control.FromHandle(Window.Handle) as System.Windows.Forms.Form))
            {

                if (keystate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                    Exit();
                else if (keystate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Up) && startY > 0)
                    startY -= 1;
                else if (keystate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Down) && startY < level.height - (GraphicsDevice.Viewport.Height / zoom))
                    startY += 1;
                else if (keystate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Left) && startX > 0)
                    startX -= 1;
                else if (keystate.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Right) && startX < level.width - (GraphicsDevice.Viewport.Width / zoom))
                    startX += 1;

                if (currentMouseState.LeftButton == Microsoft.Xna.Framework.Input.ButtonState.Pressed && form.listLayers.SelectedIndex != -1 && level.layers[form.listLayers.SelectedIndex].visible)
                {
                    if (currentMouseState.X > 0 && currentMouseState.Y > 0 && currentMouseState.X < GraphicsDevice.Viewport.Width && currentMouseState.Y < GraphicsDevice.Viewport.Height)
                    {
                        int x = (currentMouseState.X / zoom) + startX;
                        int y = (currentMouseState.Y / zoom) + startY;
                        if (x < level.width && y < level.height)
                        {
                            if (form.selection != null && form.pbPencil.BorderStyle == System.Windows.Forms.BorderStyle.Fixed3D)
                                level.layers[form.listLayers.SelectedIndex].gIDs[x + y * level.width] = form.selection.selectedItems[0];
                            else if (form.pbEraser.BorderStyle == System.Windows.Forms.BorderStyle.Fixed3D)
                                level.layers[form.listLayers.SelectedIndex].gIDs[x + y * level.width] = -1;
                            else if (form.selection != null && form.pbFillTool.BorderStyle == System.Windows.Forms.BorderStyle.Fixed3D)
                                FillTool(level.layers[form.listLayers.SelectedIndex], x,  y, level.layers[form.listLayers.SelectedIndex].gIDs[x + y * level.width], form.selection.selectedItems[0]);
                            else if (form.pbSelection.BorderStyle == System.Windows.Forms.BorderStyle.Fixed3D)
                            {
                            }
                        }
                    }
                }

                for (byte i = 0; i < level.animatedSets.Length; i++)
                    level.animatedSets[i].Update(gameTime);
            }

            base.Update(gameTime);
        }
        void FillTool(Layer layer, int x, int y, short target, short replacement)
        {
            int tile = x + y * level.width;
            if (x >= 0 && x < level.width && y >= 0 && y < level.height && layer.gIDs[tile] == target && layer.gIDs[tile] != replacement)
            {
                layer.gIDs[tile] = replacement;
                FillTool(layer, x + 1, y, target, replacement);
                FillTool(layer, x - 1, y, target, replacement);
                FillTool(layer, x, y + 1, target, replacement);
                FillTool(layer, x, y - 1, target, replacement);
            }
        }

        internal void ResizeMap(short w, short h)
        {
            for (int i = 0; i < level.layers.Length; i++)
            {
                short[] newdata = new short[w * h];
                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w; x++)
                    {
                        if (y < level.height && x < level.width)
                            newdata[x + y * w] = level.layers[i].gIDs[x + y * level.width];
                        else
                            newdata[x + y * w] = -1;
                    }
                }
                level.layers[i].gIDs = newdata;
            }
            level.width = w;
            level.height = h;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            int tileWidth = GraphicsDevice.Viewport.Width / zoom;
            int tileHeight = GraphicsDevice.Viewport.Height / zoom;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            for (int y = 0; y < tileHeight; y++)
            {
                if (y + startY >= level.height)
                    continue;
                for (int x = 0; x < tileWidth; x++)
                {

                    if (x + startX >= level.width)
                        continue;

                    int index = x + startX + (y + startY) * level.width;
                    Rectangle dest = new Rectangle(x * zoom, y * zoom, zoom, zoom);                    
                    for (int l = 0; l < level.layers.Length; l++)
                    {
                        if (!level.layers[l].visible)
                            continue;
                        int tileIdx = level.layers[l].gIDs[index];
                        if (tileIdx != -1)
                        {
                            if (level.layers[l].type == LayerType.Animated)
                            {
                                AnimatedTileSet tileSet = level.animatedSets[tileIdx];
                                int frameIndex = (tileSet.FrameIndex + x + y) % tileSet.FrameCount;
                                spriteBatch.Draw(tileSet.texture, dest, new Rectangle(frameIndex * tileSet.tileW, 0, tileSet.tileW, tileSet.tileH), Color.White);
                            }
                            else
                            {
                                TileCollection tileSet = level.getTileSetFromGID(tileIdx);
                                int setIndx = tileIdx - tileSet.firstGID;
                                spriteBatch.Draw(tileSet.texture, dest, new Rectangle((setIndx % tileSet.wTileCount) * tileSet.tileW, (setIndx / tileSet.hTileCount) * tileSet.tileH, tileSet.tileW, tileSet.tileH), Color.White);
                            }
                        }
                    }
                }
            }

            spriteBatch.Draw(mouseCursor, new Vector2(currentMouseState.X, currentMouseState.Y), Color.White);
            spriteBatch.End();

            // Grid
            effect.VertexColorEnabled = true;
            effect.Techniques[0].Passes[0].Apply();
            
            GraphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, gridVertices, 0, gridVertices.Length / 2);


            base.Draw(gameTime);
        }
    }
}
