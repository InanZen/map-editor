﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
namespace MapEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tilesetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.widthBox = new System.Windows.Forms.MaskedTextBox();
            this.heightBox = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mapSizeButton = new System.Windows.Forms.Button();
            this.mapNameLabel = new System.Windows.Forms.Label();
            this.pbEraser = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listLayers = new System.Windows.Forms.ListBox();
            this.bLayerAdd = new System.Windows.Forms.Button();
            this.bLayerRem = new System.Windows.Forms.Button();
            this.bLayerD = new System.Windows.Forms.Button();
            this.bLayerUp = new System.Windows.Forms.Button();
            this.comboLayerTypes = new System.Windows.Forms.ComboBox();
            this.tabAnimated = new System.Windows.Forms.TabControl();
            this.animatedPage = new System.Windows.Forms.TabPage();
            this.panelTileSets = new System.Windows.Forms.Panel();
            this.tabTileSets = new System.Windows.Forms.TabControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbPencil = new System.Windows.Forms.PictureBox();
            this.pbSelection = new System.Windows.Forms.PictureBox();
            this.pbFillTool = new System.Windows.Forms.PictureBox();
            this.checkLayerVisible = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLevelName = new System.Windows.Forms.TextBox();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEraser)).BeginInit();
            this.tabAnimated.SuspendLayout();
            this.panelTileSets.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPencil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFillTool)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.tilesetToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(399, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // tilesetToolStripMenuItem
            // 
            this.tilesetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.tilesetToolStripMenuItem.Name = "tilesetToolStripMenuItem";
            this.tilesetToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.tilesetToolStripMenuItem.Text = "Tileset";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addToolStripMenuItem.Text = "add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(215, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "w";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(266, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "h";
            // 
            // widthBox
            // 
            this.widthBox.Location = new System.Drawing.Point(236, 74);
            this.widthBox.Mask = "000";
            this.widthBox.Name = "widthBox";
            this.widthBox.Size = new System.Drawing.Size(24, 20);
            this.widthBox.TabIndex = 10;
            // 
            // heightBox
            // 
            this.heightBox.Location = new System.Drawing.Point(285, 74);
            this.heightBox.Mask = "000";
            this.heightBox.Name = "heightBox";
            this.heightBox.Size = new System.Drawing.Size(25, 20);
            this.heightBox.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Layers:";
            // 
            // mapSizeButton
            // 
            this.mapSizeButton.Location = new System.Drawing.Point(316, 72);
            this.mapSizeButton.Name = "mapSizeButton";
            this.mapSizeButton.Size = new System.Drawing.Size(50, 23);
            this.mapSizeButton.TabIndex = 13;
            this.mapSizeButton.Text = "Resize";
            this.mapSizeButton.UseVisualStyleBackColor = true;
            this.mapSizeButton.Click += new System.EventHandler(this.mapSizeButton_Click);
            // 
            // mapNameLabel
            // 
            this.mapNameLabel.AutoSize = true;
            this.mapNameLabel.Location = new System.Drawing.Point(65, 9);
            this.mapNameLabel.Name = "mapNameLabel";
            this.mapNameLabel.Size = new System.Drawing.Size(0, 13);
            this.mapNameLabel.TabIndex = 15;
            // 
            // pbEraser
            // 
            this.pbEraser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbEraser.Location = new System.Drawing.Point(336, 8);
            this.pbEraser.Name = "pbEraser";
            this.pbEraser.Size = new System.Drawing.Size(32, 32);
            this.pbEraser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEraser.TabIndex = 16;
            this.pbEraser.TabStop = false;
            this.pbEraser.Click += new System.EventHandler(this.pbEraser_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "eraser.png");
            this.imageList1.Images.SetKeyName(1, "fill.png");
            this.imageList1.Images.SetKeyName(2, "pencil.png");
            this.imageList1.Images.SetKeyName(3, "selection.png");
            // 
            // listLayers
            // 
            this.listLayers.FormattingEnabled = true;
            this.listLayers.Location = new System.Drawing.Point(6, 16);
            this.listLayers.Name = "listLayers";
            this.listLayers.Size = new System.Drawing.Size(137, 82);
            this.listLayers.TabIndex = 17;
            this.listLayers.SelectedIndexChanged += new System.EventHandler(this.listLayers_SelectedIndexChanged);
            this.listLayers.DoubleClick += new System.EventHandler(this.listLayers_DoubleClick);
            // 
            // bLayerAdd
            // 
            this.bLayerAdd.Location = new System.Drawing.Point(149, 35);
            this.bLayerAdd.Margin = new System.Windows.Forms.Padding(0);
            this.bLayerAdd.Name = "bLayerAdd";
            this.bLayerAdd.Size = new System.Drawing.Size(24, 20);
            this.bLayerAdd.TabIndex = 18;
            this.bLayerAdd.Text = "+";
            this.bLayerAdd.UseVisualStyleBackColor = true;
            this.bLayerAdd.Click += new System.EventHandler(this.bLayerAdd_Click);
            // 
            // bLayerRem
            // 
            this.bLayerRem.Location = new System.Drawing.Point(173, 35);
            this.bLayerRem.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.bLayerRem.Name = "bLayerRem";
            this.bLayerRem.Size = new System.Drawing.Size(24, 20);
            this.bLayerRem.TabIndex = 19;
            this.bLayerRem.Text = "-";
            this.bLayerRem.UseVisualStyleBackColor = true;
            this.bLayerRem.Click += new System.EventHandler(this.bLayerRem_Click);
            // 
            // bLayerD
            // 
            this.bLayerD.Font = new System.Drawing.Font("Moire", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLayerD.Location = new System.Drawing.Point(173, 55);
            this.bLayerD.Margin = new System.Windows.Forms.Padding(0);
            this.bLayerD.Name = "bLayerD";
            this.bLayerD.Size = new System.Drawing.Size(24, 23);
            this.bLayerD.TabIndex = 20;
            this.bLayerD.Text = "↓";
            this.bLayerD.UseVisualStyleBackColor = true;
            this.bLayerD.Click += new System.EventHandler(this.bLayerD_Click);
            // 
            // bLayerUp
            // 
            this.bLayerUp.Font = new System.Drawing.Font("Moire", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLayerUp.Location = new System.Drawing.Point(149, 55);
            this.bLayerUp.Margin = new System.Windows.Forms.Padding(0);
            this.bLayerUp.Name = "bLayerUp";
            this.bLayerUp.Size = new System.Drawing.Size(24, 23);
            this.bLayerUp.TabIndex = 21;
            this.bLayerUp.Text = "↑";
            this.bLayerUp.UseVisualStyleBackColor = true;
            this.bLayerUp.Click += new System.EventHandler(this.bLayerUp_Click);
            // 
            // comboLayerTypes
            // 
            this.comboLayerTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboLayerTypes.FormattingEnabled = true;
            this.comboLayerTypes.Location = new System.Drawing.Point(149, 81);
            this.comboLayerTypes.Name = "comboLayerTypes";
            this.comboLayerTypes.Size = new System.Drawing.Size(48, 21);
            this.comboLayerTypes.TabIndex = 22;
            this.comboLayerTypes.SelectedIndexChanged += new System.EventHandler(this.comboLayerTypes_SelectedIndexChanged);
            // 
            // tabAnimated
            // 
            this.tabAnimated.Controls.Add(this.animatedPage);
            this.tabAnimated.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabAnimated.Enabled = false;
            this.tabAnimated.Location = new System.Drawing.Point(0, 0);
            this.tabAnimated.Name = "tabAnimated";
            this.tabAnimated.SelectedIndex = 0;
            this.tabAnimated.Size = new System.Drawing.Size(375, 234);
            this.tabAnimated.TabIndex = 1;
            this.tabAnimated.Visible = false;
            // 
            // animatedPage
            // 
            this.animatedPage.Location = new System.Drawing.Point(4, 22);
            this.animatedPage.Name = "animatedPage";
            this.animatedPage.Padding = new System.Windows.Forms.Padding(3);
            this.animatedPage.Size = new System.Drawing.Size(367, 208);
            this.animatedPage.TabIndex = 1;
            this.animatedPage.Text = "Animated";
            this.animatedPage.UseVisualStyleBackColor = true;
            // 
            // panelTileSets
            // 
            this.panelTileSets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTileSets.Controls.Add(this.tabTileSets);
            this.panelTileSets.Controls.Add(this.tabAnimated);
            this.panelTileSets.Location = new System.Drawing.Point(12, 136);
            this.panelTileSets.Name = "panelTileSets";
            this.panelTileSets.Size = new System.Drawing.Size(375, 234);
            this.panelTileSets.TabIndex = 24;
            // 
            // tabTileSets
            // 
            this.tabTileSets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabTileSets.Location = new System.Drawing.Point(0, 0);
            this.tabTileSets.Name = "tabTileSets";
            this.tabTileSets.SelectedIndex = 0;
            this.tabTileSets.Size = new System.Drawing.Size(375, 234);
            this.tabTileSets.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pbPencil);
            this.panel1.Controls.Add(this.pbSelection);
            this.panel1.Controls.Add(this.pbFillTool);
            this.panel1.Controls.Add(this.checkLayerVisible);
            this.panel1.Controls.Add(this.listLayers);
            this.panel1.Controls.Add(this.comboLayerTypes);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.bLayerUp);
            this.panel1.Controls.Add(this.textBoxLevelName);
            this.panel1.Controls.Add(this.bLayerAdd);
            this.panel1.Controls.Add(this.bLayerRem);
            this.panel1.Controls.Add(this.bLayerD);
            this.panel1.Controls.Add(this.pbEraser);
            this.panel1.Controls.Add(this.mapSizeButton);
            this.panel1.Controls.Add(this.widthBox);
            this.panel1.Controls.Add(this.heightBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(13, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(374, 103);
            this.panel1.TabIndex = 25;
            // 
            // pbPencil
            // 
            this.pbPencil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPencil.Location = new System.Drawing.Point(222, 8);
            this.pbPencil.Name = "pbPencil";
            this.pbPencil.Size = new System.Drawing.Size(32, 32);
            this.pbPencil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPencil.TabIndex = 26;
            this.pbPencil.TabStop = false;
            this.pbPencil.Click += new System.EventHandler(this.pbPencil_Click);
            // 
            // pbSelection
            // 
            this.pbSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSelection.Location = new System.Drawing.Point(260, 8);
            this.pbSelection.Name = "pbSelection";
            this.pbSelection.Size = new System.Drawing.Size(32, 32);
            this.pbSelection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSelection.TabIndex = 25;
            this.pbSelection.TabStop = false;
            this.pbSelection.Click += new System.EventHandler(this.pbSelection_Click);
            // 
            // pbFillTool
            // 
            this.pbFillTool.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbFillTool.Location = new System.Drawing.Point(298, 8);
            this.pbFillTool.Name = "pbFillTool";
            this.pbFillTool.Size = new System.Drawing.Size(32, 32);
            this.pbFillTool.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFillTool.TabIndex = 24;
            this.pbFillTool.TabStop = false;
            this.pbFillTool.Click += new System.EventHandler(this.pbFillTool_Click);
            // 
            // checkLayerVisible
            // 
            this.checkLayerVisible.AutoSize = true;
            this.checkLayerVisible.Location = new System.Drawing.Point(149, 18);
            this.checkLayerVisible.Margin = new System.Windows.Forms.Padding(0);
            this.checkLayerVisible.Name = "checkLayerVisible";
            this.checkLayerVisible.Size = new System.Drawing.Size(56, 17);
            this.checkLayerVisible.TabIndex = 23;
            this.checkLayerVisible.Text = "Visible";
            this.checkLayerVisible.UseVisualStyleBackColor = true;
            this.checkLayerVisible.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Lvl name:";
            // 
            // textBoxLevelName
            // 
            this.textBoxLevelName.Location = new System.Drawing.Point(274, 46);
            this.textBoxLevelName.Name = "textBoxLevelName";
            this.textBoxLevelName.Size = new System.Drawing.Size(92, 20);
            this.textBoxLevelName.TabIndex = 17;
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.removeToolStripMenuItem.Text = "remove";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 404);
            this.ControlBox = false;
            this.Controls.Add(this.mapNameLabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelTileSets);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(415, 420);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEraser)).EndInit();
            this.tabAnimated.ResumeLayout(false);
            this.panelTileSets.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPencil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFillTool)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem newToolStripMenuItem;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;
        private OpenFileDialog openFileDialog1;
        private Label label1;
        private Label label2;
        private MaskedTextBox widthBox;
        private MaskedTextBox heightBox;
        private Label label3;
        private Button mapSizeButton;
        private Label mapNameLabel;
        private ImageList imageList1;
        public ListBox listLayers;
        private Button bLayerAdd;
        private Button bLayerRem;
        private Button bLayerD;
        private Button bLayerUp;
        public ComboBox comboLayerTypes;
        public TabControl tabAnimated;
        public TabPage animatedPage;
        private Panel panelTileSets;
        public TabControl tabTileSets;
        private Panel panel1;
        private Label label4;
        private TextBox textBoxLevelName;
        private CheckBox checkLayerVisible;
        internal PictureBox pbPencil;
        internal PictureBox pbSelection;
        internal PictureBox pbFillTool;
        internal PictureBox pbEraser;
        private ToolStripMenuItem tilesetToolStripMenuItem;
        private ToolStripMenuItem addToolStripMenuItem;
        internal ToolStripMenuItem removeToolStripMenuItem;

    }
}